const axios = require('axios')
const cheerio = require('cheerio')
const fs = require('fs')
const util = require('util')

const readFile = util.promisify(fs.readFile);

const outputFileName = 'output.json'
const titlePostfixes = [
  ' [dt./OV]',
  ' [OmU]',
  ' [OV/OmU]',
  ' [OV]',
  ' (4K UHD)',
  ' (Deutsche Untertitel)',
  ' (Extended Edition)',
  ' (German Version)',
  ' (OmU)',
  ' - Extended Edition',
  ' - Unmaskierte Filmversion',
]

const inputFiles = process.argv.slice(process.argv.indexOf(__filename) + 1)
const { TMDB_API_KEY } = process.env

if (!TMDB_API_KEY) {
  throw new Error('Missing environment variable TMDB_API_KEY!')
}

const mapping = fs.existsSync(outputFileName) ? JSON.parse(fs.readFileSync(outputFileName, 'utf-8')) : []

function parseInputFile (fileName) {
  console.log(`Parsing ${fileName} ...`)
  return readFile(fileName)
    .then(fileContent => {
      const $ = cheerio.load(fileContent)
      const detailsLinks = $('.s-access-detail-page')

      detailsLinks.each(function () {
        const link = $(this)
        const url = link.attr('href').replace(/\/[^/]*\/dp\//, '/dp/')

        let title = link.text().normalize('NFC')
        titlePostfixes.forEach(postfix => { title = title.replace(postfix, '') })
        titlePostfixes.forEach(postfix => { title = title.replace(postfix, '') })

        const year = link.siblings().eq(2).text()

        let entry = mapping.find(entry => entry.url === url)
        if (!entry) {
          console.log(`Adding new entry for ${url}`)
          entry = { tmdbId: null, url, title, year, originalTitle: null }
          mapping.push(entry)
        }

        const infoBox = link.parents('.a-fixed-left-grid-col')
        entry.isFreeForPrime = /EUR 0,00\s*Mit einem Prime Abo ansehen/.test(infoBox.text())
      })

      console.log(`Finished parsing ${fileName}.`)
    })
}

function findTMDBResult(params) {
  console.log(`Looking up "${params.query}" on themoviedb.org`)

  return axios.get('https://api.themoviedb.org/3/search/movie', { params })
    .then(({ data }) => {
      const { query } = params
      let { results } = data

      if (results.length === 0) {
        if (query.includes('-')) {
         const reducedQuery = query.replace(/ ?-.*$/, '')
         console.log(`"${query}" returned no result, retrying with "${reducedQuery}" ...`)
         return findTMDBResult({
           ...params,
           query: reducedQuery
         })
        }

        if (query.includes('.')) {
         const reducedQuery = query.replace(/\./, ' ')
         console.log(`"${query}" returned no result, retrying with "${reducedQuery}" ...`)
         return findTMDBResult({
           ...params,
           query: reducedQuery
         })
        }

        if (query.endsWith(')')) {
         const reducedQuery = query.replace(/ ?\(.*\)$/, '')
         console.log(`"${query}" returned no result, retrying with "${reducedQuery}" ...`)
         return findTMDBResult({
           ...params,
           query: reducedQuery
         })
        }
      }

      if (results.length > 1) {
        console.log('Result was not unique, filtering by title')
        results = results.filter(({ title }) => title.toLowerCase() === query.toLowerCase())

        if (results.length === 0) {
          console.log('Filtering by title failed, trying original title')
          results = data.results.filter(({ original_title }) => original_title.toLowerCase() === query.toLowerCase())
        }

        if (results.length === 0) {
         console.log('Filtering by original title failed, trying release date')
         results = data.results.filter(({ release_date }) => release_date.startsWith(params.year))
        }
      }

      if (results.length > 1) {
        console.log('Result was not unique, filtering by release date')
        results = results.filter(({ release_date }) => release_date.startsWith(params.year))
      }

      return results
    })
}

function fetchTMDBId(entry) {
  const existingEntry = mapping.find(({ url }) => entry.url === url)
  if (existingEntry && existingEntry.tmdbId) {
    console.log(`Reusing existing entry of ${entry.url}`)
    return Promise.resolve()
  }

  const params = {
    api_key: TMDB_API_KEY,
    include_adult: false,
    language: 'de',
    query: entry.title,
    year: entry.year
  }

  console.log(`Fetching TMDB ID for ${entry.url}`)
  return findTMDBResult(params)
    .then(results => {
      if (results.length === 0) {
        console.log(`No result for ${JSON.stringify(entry)}!`)
      } else if (results.length > 1) {
        console.log(`Result for ${JSON.stringify(entry)} is not unique!`)
      } else {
        const { id: tmdbId, title, original_title: originalTitle } = results[0]
        console.log(`Setting TMDB ID for ${entry.url} to ${tmdbId} ("${title}" / "${originalTitle}")`)
        Object.assign(entry, {
          tmdbId,
          title,
          originalTitle
        })
        writeMappings()
      }
    })
}

function writeMappings () {
  mapping.sort((a, b) => (a.tmdbId || 0) - (b.tmdbId || 0))
  fs.writeFileSync(outputFileName, JSON.stringify(mapping, null, 2))
}

Promise.all(inputFiles.map(parseInputFile))
  .then(writeMappings)
  .then(() => Promise.all(mapping.map(fetchTMDBId)))

